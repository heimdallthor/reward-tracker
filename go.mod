module gitlab.com/heimdall/reward-tracker

go 1.15

require (
	github.com/binance-chain/go-sdk v1.2.6 // indirect
	github.com/cosmos/cosmos-sdk v0.39.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/thorchain/thornode v0.19.1-0.20210127025303-d32f258d18ae
)

replace (
	github.com/binance-chain/go-sdk => gitlab.com/thorchain/binance-sdk v1.2.2
	github.com/binance-chain/tss-lib => gitlab.com/thorchain/tss/tss-lib v0.0.0-20201118045712-70b2cb4bf916
)
