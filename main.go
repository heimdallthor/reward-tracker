package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func setupConfig() {
	// Read in the configuration file for the sdk
	nw := common.GetCurrentChainNetwork()
	switch nw {
	case common.TestNet, common.MockNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("tthor", "tthorpub")
		config.SetBech32PrefixForValidator("tthorv", "tthorvpub")
		config.SetBech32PrefixForConsensusNode("tthorc", "tthorcpub")
		config.Seal()
	case common.MainNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("thor", "thorpub")
		config.SetBech32PrefixForValidator("thorv", "thorvpub")
		config.SetBech32PrefixForConsensusNode("thorc", "thorcpub")
		config.Seal()
	}
}
func main() {
	setupConfig()
	app := &cli.App{
		Name:  "reward-tracker",
		Usage: "this tool is to track node operator's award",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "server",
				Aliases:     []string{"s"},
				Usage:       "server url or ip address",
				EnvVars:     []string{"THORNODE_IP"},
				Value:       "localhost",
				Required:    true,
				Hidden:      false,
				HasBeenSet:  true,
				Destination: &thornodeIP,
			},
			&cli.IntFlag{
				Name:        "port",
				Aliases:     []string{"p"},
				Usage:       "port",
				EnvVars:     []string{"SERVER_PORT"},
				Value:       1317,
				Required:    true,
				HasBeenSet:  true,
				Destination: &thornodePort,
			},
			&cli.IntFlag{
				Name:       "interval",
				Usage:      "interval to retrieve the reward, in seconds",
				Value:      5,
				Required:   true,
				HasBeenSet: true,
			},
			&cli.StringFlag{
				Name:       "address",
				Usage:      "node address need to be monitored",
				Required:   true,
				Hidden:     false,
				Value:      "",
				HasBeenSet: false,
			},
		},
		Action: appAction,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

var (
	thornodeIP   string
	thornodePort int
)

func appAction(ctx *cli.Context) error {
	nodeAddress := ctx.String("address")
	if nodeAddress == "" {
		return fmt.Errorf("please provide node address need to monitor using --address")
	}
	interval := ctx.Int("interval")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	defer func() {
		signal.Stop(c)
	}()
	path := fmt.Sprintf("/nodeaccount/%s", nodeAddress)
	for {
		select {
		case <-time.After(time.Second * time.Duration(interval)):
			blockHeight, err := getLastBlock()
			if err != nil {
				fmt.Println(err)
				continue
			}
			vaultData, err := getVault()
			if err != nil {
				fmt.Println(err)
				continue
			}

			buf, err := getFromThorchain(path)
			if err != nil {
				return fmt.Errorf("fail to get node account info from thornode: %w", err)
			}
			var nodeAccount types.QueryNodeAccount
			if err := codec.Cdc.UnmarshalJSON(buf, &nodeAccount); err != nil {
				return fmt.Errorf("fail to unmarshal node account: %w", err)
			}
			blockEarned := blockHeight - nodeAccount.ActiveBlockHeight
			totalBlockEarned := blockEarned - nodeAccount.SlashPoints
			share := float64(totalBlockEarned) / float64(vaultData.TotalBondUnits.Uint64())
			fmt.Printf("%s,%s,%d,%d,%s,%.6f\n", vaultData.TotalBondUnits.String(), vaultData.BondRewardRune.QuoUint64(common.One), blockEarned, nodeAccount.SlashPoints, nodeAccount.CurrentAward.QuoUint64(common.One), share)
		case <-c:
			break
		}
	}
	return nil
}
func getVault() (types.VaultData, error) {
	buf, err := getFromThorchain("vault")
	if err != nil {
		return types.VaultData{}, fmt.Errorf("fail to get from thorchain:%w", err)
	}
	var result types.VaultData
	if err := json.Unmarshal(buf, &result); err != nil {
		return types.VaultData{}, fmt.Errorf("fail to unmarshal vault data: %w", err)
	}
	return result, nil
}
func getLastBlock() (int64, error) {
	buf, err := getFromThorchain("lastblock")
	if err != nil {
		return 0, fmt.Errorf("fail to get last block from thorchain: %w", err)
	}
	type QueryResHeights struct {
		Chain            common.Chain `json:"chain"`
		LastChainHeight  int64        `json:"lastobservedin,string"`
		LastSignedHeight int64        `json:"lastsignedout,string"`
		Thorchain        int64        `json:"thorchain,string"`
	}
	var result QueryResHeights
	if err := json.Unmarshal(buf, &result); err != nil {
		return 0, fmt.Errorf("fail to unmarshal last block from thorchain: %w", err)
	}
	return result.Thorchain, nil
}

func getFromThorchain(path string) ([]byte, error) {
	url := fmt.Sprintf("http://%s:%d/thorchain/%s", thornodeIP, thornodePort, path)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("fail to call %s :%w", url, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("fail to read response body: %w", err)
	}
	return body, nil
}
